package treenityProtocol

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	core "gitlab.com/txt-dev-pub/treenity-core"
	"go.uber.org/zap"
	"runtime/debug"
)

type baseSubClient struct {
	logger     *zap.Logger
	sender     Sender
	subscriber Subscriber
}

func NewBaseSubClient(logger *zap.Logger, sender Sender, subscriber Subscriber) ISubscriptionClient {
	return &baseSubClient{
		logger:     logger,
		sender:     sender,
		subscriber: subscriber,
	}
}

func (s *baseSubClient) Proxy(ctx context.Context, url string, params core.Data) Socket {
	socket := s.Sub(ctx, url, true, true, true, BypassWrapper)
	socket.sink <- params
	return socket
}

func (s *baseSubClient) Subscribe(ctx context.Context, url string, params core.Data) Socket {
	socket := s.Sub(ctx, url, true, true, false, NewPatchWrapper)
	socket.sink <- params
	return socket
}

func (s *baseSubClient) Request(ctx context.Context, url string, params core.Data) core.Data {
	// TODO Remove it
	socket := s.Sub(ctx, url, false, false, false, NewSimpleWrapper)
	socket.sink <- params

	v := <-socket.source
	return v
}

func (s *baseSubClient) runInput(ctx context.Context, subId string, val chan core.Data, ch <-chan ProtocolMessage, wrapper Wrapper) {
	for {
		select {
		case <-ctx.Done():
			return
		case msg, ok := <-ch:
			if !ok {
				s.logger.Warn(
					"Client IN channel has been closed",
					zap.String("subId", subId),
				)
				return
			}

			val <- wrapper.In(msg)
		}
	}
}

func (s *baseSubClient) runOutput(ctx context.Context, retId string, val chan core.Data, wrapper Wrapper) {
	for {
		select {
		case <-ctx.Done():
			return
		case msg, ok := <-val:
			if !ok {
				s.logger.Warn(
					"Client OUT channel has been closed",
					zap.String("retId", retId),
				)
				return
			}

			err := s.sender(retId, wrapper.Out(msg))
			if err != nil {
				s.logger.Error(
					"runOutput.sender error",
					zap.String("retId", retId),
				)
			}
		}
	}
}

func (s *baseSubClient) Sub(baseCtx context.Context, url string, subcli, subsrv, proxy bool, wBuilder WrapperBuilder) Socket {
	ctx, closeFn := context.WithCancel(baseCtx)
	subId := fmt.Sprintf("C-%s", uuid.New().String())
	wrapper := wBuilder()

	inputCh := make(chan ProtocolMessage, 30)
	input := make(chan core.Data, 30)
	output := make(chan core.Data, 30)

	go func() {
		defer func() {
			if err := recover(); err != nil {
				fmt.Println("🛑 Client: ", err)
				debug.PrintStack()
				closeFn()
			}
		}()

		requestMsg := <-output
		s.logger.Info(
			"🙀 Client subscription",
			zap.String("url", url),
			zap.String("subId", subId),
			zap.Bool("subcli", subcli),
			zap.Bool("subsrv", subsrv),
		)

		// Subscribe on local channel and send Sub message to server to execute method
		go s.subscriber(ctx, subId, inputCh)

		retMsg := wrapper.Out(requestMsg)
		err := s.sender(url, subUpdate(&subId, retMsg.AsString(), subcli, subsrv))
		if err != nil {
			s.logger.Error(
				"🛑 Error from server on sub",
				zap.String("url", url),
				zap.String("subId", subId),
				zap.String("err", err.Error()),
			)
			//TODO Close input/output
			output <- core.NodeDataErr("error-on-sub:" + err.Error())
			closeFn()
			return
		}

		// Getting Ret response from service Subscription
		m := <-inputCh

		// Getting initial message from sub
		s.logger.Info(
			"⮑ Return message from server",
			zap.Any("msg", m),
		)

		var errMsg *string
		var retId *string

		switch m.Op() {
		case UnsubscriptionMessageType:
			input <- core.NodeDataErr(*m.Error())
		case ReturnMessageType:
			// Set Channel IN|OUT variables
			// TODO Check it

			if proxy {
				subcli = true
				subsrv = true
			} else {
				subcli = subsrv && m.Subcli()
				subsrv = subcli && m.Subsrv()
			}

			// Getting RetID to use it in changing server request
			retId = m.Sub()
			if m == nil || (retId == nil && subsrv) {
				s.logger.Error(
					"🛑 No ret message from server",
					zap.String("url", url),
					zap.String("subId", subId),
				)
				output <- core.NodeDataErr("no-ret-message-from-server")
				closeFn()
				return
			}

			// Run IN|OUT pipes
			if subcli {
				go s.runInput(ctx, subId, input, inputCh, wrapper)
			}

			if subsrv {
				go s.runOutput(ctx, *retId, output, wrapper)
			}

			// initial value to run
			if m != nil {
				input <- wrapper.In(baseMsg(m.Value()))
			}

			if !subcli && !subsrv {
				closeFn()
			}

			// Wait to close the socket to stop
			<-ctx.Done()
		default:
			err := "Unknown operation for current stage"
			errMsg = &err
		}

		if retId != nil && subcli {
			s.sender(*retId, Unsub(errMsg, &subId))
		}

		s.logger.Info("Removed client sub")

		close(input)
		close(output)
		close(inputCh)
	}()

	return Socket{
		ctx:     ctx,
		closeFn: closeFn,
		id:      subId,
		source:  input,
		sink:    output,
		url:     url,
		wrapper: wrapper,
	}
}
