v1.8.10
* Added check if child is nil

v1.8.9
* Fixed patch out method

v1.8.8
* Updated core lib

v1.8.7
* Updated core
* Fixed unsubscribe on connection error
* Displaying errors
* Added resolver helper

v1.8.6
* Fixed service provider

v1.8.5
* Updated core lib
* Added service manager
* added method link struct

v1.8.4
* Fixed TXT_CALLBACK_METHOD with sending to repo

v1.8.3
* Fixed patch and request wrappers

v1.8.2
* Ignoring empty messages

v1.8.1
* Fixed bypass wrapper

v1.8.0
* Added bypass proxy
* moved to nats publish from request

v1.7.5
* Fixed subsrv sub cli

v1.7.4
* Removed wrapper from params

v1.7.3
* Added subcli and subsrv to params

v1.7.2
* Fixed server runner to return zero methods

v1.7.1
* Added nats service runner

v1.7.0
* Created wildcard methods
* Tested protocol

v1.6.1
* Removed nats server wrap
* Fixed examples
* Removed service manager, replaced by method resolver
* Fixed subcli and subsrv
* Added mutex for base server class
* Updated core version

v1.6.0
* Added NATS server
* rethink broadcast methods

v1.5.1
* Changed chin and chout to subcli and subsrv

v1.5.0
* Updated core lib to 1.6.0

v1.4.15
* Updated core lib to 1.5.19

v1.4.14
* Fixed displaying errors

v1.4.13
* Updated core

v1.4.12
* Updated core function and removed unused code

v1.4.11
* Added params for service

v1.4.10
* Added panic recover for methods

v1.4.9
* Reverted changes

v1.4.7
* Fixed remote loader. Added possibility to display meta

v1.4.6
* Updated loader protocol

v1.4.5
* Core lib updated

v1.4.4
* Fixed multi thread subs reading

v1.4.3
* Fixed process link in loader

v1.4.2
* Updated core lib

v1.4.1
* Added error return in SimpleWrap

v1.4.0
* Changed Params to Data

v1.3.13
* Fixed children method. Added node link to meta

v1.3.12
* Added children method

v1.3.11
* Added error response

v1.3.10
* Fixed NATS timeout
* Fixed request method, when error is happened

v1.3.9
* Added getting client method from service helper

v1.3.8
* Added remote loader

v1.3.7
* Fixed broadcast service register

v1.3.6
* Fixed broadcast service register

v1.3.4
* Added broadcast service register

v1.3.3
* Fixed register service

v1.3.2
* Adding waiting to connect to NATS
* Removed panic when NATS_SERVER_DURATION is emppty

v1.3.1
* Changed env name

v1.3.0
* Added service client

v1.2.2
* Added url to runner

v1.2.1
* Fixed simple wrapper

v1.2.0
* Communication based on Params and Data models

v1.1.10
* Added temporary wrapper

v1.1.9
* Added check on empty message

v1.1.8
* Fixed AddMethod visibility

v1.1.7
* Added ListMethods method
* AddMethod moved to public

v1.1.6
* Changed method to getting link

v1.1.5
* updated core lib to v1.0.8

v1.1.4
* Added return message info

v1.1.3
* updated core lib to v1.0.7

v1.1.2
* hidden baseSubServer struct
* updated core lib to v1.0.6

v1.1.1
* Added missing parameter

v1.1.0
* Changed model
* Added logging zap lib
* Added core methods for service and methods

v1.0.4
* Fixed client method execution
* Fixed request method

v1.0.0
* Protocol init