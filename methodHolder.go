package treenityProtocol

type MethodHolder interface {
	AddMethod(name string, method Method)
	RemoveMethod(name string)
	GetMethod(name string) (Method, bool)
}
