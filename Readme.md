```
________                       __________             ________             _____            _________      
___  __/__________________________(_)_  /_____  __    ___  __ \______________  /______      __  ____/_____ 
__  /  __  ___/  _ \  _ \_  __ \_  /_  __/_  / / /    __  /_/ /_  ___/  __ \  __/  __ \     _  / __ _  __ \
_  /   _  /   /  __/  __/  / / /  / / /_ _  /_/ /     _  ____/_  /   / /_/ / /_ / /_/ /     / /_/ / / /_/ /
/_/    /_/    \___/\___//_/ /_//_/  \__/ _\__, /      /_/     /_/    \____/\__/ \____/      \____/  \____/ 
                                         /____/                                                            
```

## Server client to connect by ```Treenity Protocol```
A Go library for using Treenity protocol

### What is Treenity Protocol
It is an application protocol which using JSON and have next structure
```json
{
  "msg": "sub",
  "value": "any value",
  "ret": "return sub id",
  "sid": "sub id"
}
```
* ```msg``` - message type
* ```value``` - payload
* ```ret``` - id that the client and server use to create a subscription
* ```sid``` - subscription id
### Available message types
* ```sub``` - subscribe to a URL
* ```unsub``` - unsubscribe
* ```next``` - update a stream params
* ```patched``` - receiving changes from a server

## Messages for subscription manage
### Start a subscription
```json
{  "msg": "sub", "ret": "subscriptionId", "value": PubParams, }
```
### Stop a subscription
```json
{ "msg": "unsub"}
```
### Update a subscription
```json
{ "msg": "next", "value": PubParams}
```
### Error on a subscription failed
```json
{ "msg": "unsub", "error": String}
```

### Service manage
// TODO fill it

### Run test
Run nats container before start
```bash
docker run -p 80:80 sphqxe/nats-webui
docker run -p 4222:4222 -p 6222:6222 -p 8222:8222 nats:2.9.2-alpine3.16
```