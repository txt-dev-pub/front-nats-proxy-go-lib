package treenityProtocol

import (
	"context"
	"errors"
	"fmt"
	core "gitlab.com/txt-dev-pub/treenity-core"
)

type ServiceManager interface {
	Register(ctx context.Context, serviceName string, service core.Runnable, params core.Data) error
	Unregister(link string)
	ListMethods() []MethodLink
	MethodProvider(ctx context.Context, in core.Data, out core.Pipe)
	Resolve(ctx context.Context, in core.Data, out core.Pipe)
}

type serviceManager struct {
	server       ISubscriptionServer
	services     map[string]context.CancelFunc
	methodsNames map[string]string
	methods      M
	methodsLinks []MethodLink
}

func (s *serviceManager) ListMethods() []MethodLink {
	return s.methodsLinks
}

func (s *serviceManager) Register(ctx context.Context, serviceName string, service core.Runnable, params core.Data) error {
	pipe := make(chan core.Data)
	go service(ctx, params, pipe)

	result := <-pipe
	methods := core.NodeDataValue[M](result, "methods")

	if methods == nil {
		return errors.New("no-methods")
	}

	for fieldName, methodHandler := range *methods {
		var methodName string
		if len(serviceName) == 0 {
			methodName = fmt.Sprintf("#%s", fieldName)
		} else {
			methodName = fmt.Sprintf("$%s#%s", serviceName, fieldName)
		}
		s.methods[methodName] = methodHandler
		fmt.Printf("\t➡️ Registred method: \033[32m%s\033[0m\n", methodName)
	}

	return nil
}

func (s *serviceManager) AddMethodWithWrapper(ctx context.Context, methodUrl string, method core.Runnable, wBuilder WrapperBuilder, subcli, subsrv bool) {
	methodName := core.Link(methodUrl).Field()
	s.methods[methodName] = Method{
		Instance:       method,
		Subsrv:         subsrv,
		Subcli:         subcli,
		WrapperBuilder: wBuilder,
	}
}

func (s *serviceManager) MethodProvider(ctx context.Context, in core.Data, out core.Pipe) {
	out <- core.Data{"methods": s.methodsLinks}
}

func (s *serviceManager) Resolve(ctx context.Context, in core.Data, out core.Pipe) {
	url := in.GetLink("url")

	fieldName := url.Field()
	metaName := url.Meta()

	var methodName string
	if len(metaName) == 0 {
		methodName = fmt.Sprintf("#%s", fieldName)
	} else {
		methodName = fmt.Sprintf("$%s#%s", metaName, fieldName)
	}

	if h, ok := s.methods[methodName]; ok {
		out <- core.Data{"method": h}
		return
	}

	out <- core.NodeDataErr("service-manager.resolving.method-not-found")
}

func (s *serviceManager) Unregister(link string) {
	panic("Implement me")
	if cancel, ok := s.services[link]; ok {
		cancel()
		delete(s.services, link)
	}
}

func NewServiceManager() ServiceManager {
	return &serviceManager{
		services:     make(map[string]context.CancelFunc),
		methodsNames: make(map[string]string),
		methods:      make(M),
	}
}
