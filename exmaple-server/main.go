package main

import (
	"context"
	tp "gitlab.com/txt-dev-pub/treenity-protocol"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	os.Setenv("DEBUG", "TRUE")
	os.Setenv("NATS_SERVER", "0.0.0.0:4222")
	os.Setenv("TXT_URL", "txt:/test.*")

	sc := tp.NewServiceClient(nil)
	err := sc.Register(context.TODO(), "txt:/test.service", "orders", TestService())
	if err != nil {
		panic(err)
	}

	if err != nil {
		panic(err)
	}

	// Press Ctrl+C to exit the process
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt, syscall.SIGTERM)
	<-ch
}
