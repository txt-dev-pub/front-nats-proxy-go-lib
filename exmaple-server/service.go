package main

import (
	"context"
	"fmt"
	core "gitlab.com/txt-dev-pub/treenity-core"
	tp "gitlab.com/txt-dev-pub/treenity-protocol"
	"time"
)

func testMethod() core.Runnable {
	return func(ctx context.Context, in core.Data, out core.Pipe) {
		ticker := time.NewTicker(1 * time.Second)
		fmt.Printf("🥳 Runned test method with value: %s\n\n", in)

		go func() {
			time.Sleep(2 * time.Second)
			for {
				select {
				case <-ctx.Done():
					fmt.Println("quit")
					ticker.Stop()
					return
				case <-ticker.C:
					out <- map[string]any{
						"test": time.Now().Unix(),
					}
				}
			}
		}()
	}
}

func respMethod() core.Runnable {
	return func(ctx context.Context, in core.Data, out core.Pipe) {
		fmt.Printf("🥳 Runned resp method with value: %s\n", in)

		out <- map[string]any{
			"result": "true",
		}
	}
}

func TestService() core.Runnable {
	return tp.CreateService(tp.M{
		"test": tp.CreatePatchMethod(testMethod(), true, true),
		"resp": tp.CreateSimpleMethod(respMethod()),
	}, nil)
}
