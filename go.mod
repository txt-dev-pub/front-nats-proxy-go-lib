module gitlab.com/txt-dev-pub/treenity-protocol

go 1.19

require (
	github.com/evanphx/json-patch/v5 v5.6.0
	github.com/google/uuid v1.3.0
	github.com/mitchellh/mapstructure v1.5.0
	github.com/nats-io/nats.go v1.22.1
	github.com/stretchr/testify v1.8.1
	github.com/tidwall/gjson v1.14.4
	github.com/tidwall/sjson v1.2.5
	github.com/wI2L/jsondiff v0.3.0
	gitlab.com/txt-dev-pub/treenity-core v1.7.6
	go.uber.org/zap v1.23.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/lithammer/shortuuid/v4 v4.0.0 // indirect
	github.com/nats-io/nats-server/v2 v2.8.4 // indirect
	github.com/nats-io/nkeys v0.3.0 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/signature v0.0.0-20160104132143-168b2a1e1b56 // indirect
	github.com/stretchr/stew v0.0.0-20130812190256-80ef0842b48b // indirect
	github.com/stretchr/tracer v0.0.0-20140124184152-66d3696bba97 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.1 // indirect
	gitlab.com/nikita.morozov/ms-shared v1.11.0 // indirect
	go.uber.org/atomic v1.10.0 // indirect
	go.uber.org/multierr v1.8.0 // indirect
	golang.org/x/crypto v0.5.0 // indirect
	golang.org/x/exp v0.0.0-20230213192124-5e25df0256eb // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	gorm.io/gorm v1.24.5 // indirect
)
