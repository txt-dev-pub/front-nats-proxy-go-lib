package treenityProtocol

import (
	"context"
	"github.com/mitchellh/mapstructure"
	core "gitlab.com/txt-dev-pub/treenity-core"
)

type structProvider struct {
	client  ISubscriptionClient
	baseUrl string
}

func (s *structProvider) Get(data core.Link) *core.TreeNode {
	res := s.client.Request(context.TODO(), string(data.SetHost(s.baseUrl).AddField("item")), core.Data{})
	return res.GetNode("item")
}

// TODO: FIX IT Overhead
func (s *structProvider) Children(data core.Link) []*core.TreeNode {
	res := s.client.Request(context.TODO(), string(data.SetHost(s.baseUrl).AddField("children")), core.Data{})

	switch v := res["children"].(type) {
	case []any:
		var arr []*core.TreeNode
		for _, c := range v {
			var result core.TreeNode
			err := mapstructure.Decode(c, &result)
			if err != nil {
				continue
			}

			props := result.GetProps()
			for _, p := range props.Metas {
				p.Node = &result
			}
			result.SetData(props)

			arr = append(arr, &result)
		}

		return arr
	}

	return []*core.TreeNode{}
}

func RemoteLoader(baseUrl string, client ISubscriptionClient) core.Loader {
	return &structProvider{
		client:  client,
		baseUrl: baseUrl,
	}
}
