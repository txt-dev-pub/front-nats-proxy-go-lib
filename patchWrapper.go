package treenityProtocol

import (
	"encoding/json"
	pjson "github.com/evanphx/json-patch/v5"
	"github.com/tidwall/gjson"
	"github.com/tidwall/sjson"
	"github.com/wI2L/jsondiff"
	core "gitlab.com/txt-dev-pub/treenity-core"
)

type patchWrapper struct {
	value    string
	outValue core.Data
}

func patchValue(value string, patches string) core.Data {
	if value == "{}" {
		// TODO: Check path is ""
		if gjson.Get(patches, "0.op").String() == "add" {
			patches, _ = sjson.Set(patches, "0.op", "replace")
		}
	}

	patch, err := pjson.DecodePatch([]byte(patches))
	if err != nil {
		panic(err)
	}

	modified, err := patch.Apply([]byte(value))
	if err != nil {
		panic(err)
	}

	value = string(modified)

	res, ok := gjson.ParseBytes(modified).Value().(map[string]interface{})
	if !ok {
		//TODO: Error parse data
		panic("Bugagagaga")
	}

	return res
}

func (w *patchWrapper) In(msg ProtocolMessage) core.Data {
	switch msg.Op() {
	case UnsubscriptionMessageType:
		return core.Data{}
	case SubscriptionMessageType:
		res, ok := gjson.Parse(msg.Value()).Value().(map[string]interface{})
		if !ok {
			panic("mew")
		}
		return res
	case PatchedMessageType:
		return patchValue(msg.Value(), w.value)
	}

	//patch, err := jsonpatch.CreateMergePatch([]byte(w.value), msg.AsBytes())
	//if err != nil {
	//	panic(err)
	//}

	res, ok := gjson.Parse(w.value).Value().(map[string]interface{})
	if !ok {
		//TODO: Error parse data
		panic("Bugagagaga")
	}

	return res
}

// TODO
// func (w *patchWrapper) Out(data core.Data) ProtocolMessage {
//	jsonValue, _ := json.Marshal(data)
//
//	//if len(w.current) == 0 {
//	//	w.current = jsonValue
//	//	return AddMessage("/", string(jsonValue))
//	//}
//
//	patch, err := jsondiff.CompareJSONOpts(w.current, jsonValue, jsondiff.Rationalize())
//	if err != nil {
//		errText := err.Error()
//		fmt.Println("🛑 patch error: ", errText)
//		return Unsub(&errText, nil)
//	}
//
//	if patch == nil {
//		return nil
//	}
//
//	strPatch, _ := json.Marshal(patch)
//	if len(strPatch) == 0 {
//		return BatchMessage("[]")
//	}
//
//	//if bytes.Equal(w.current, []byte("{}")) && len(patch) > 0 && patch[0].Type == "add" {
//	//	strPatch, _ = sjson.SetBytes(strPatch, "0.op", "replace")
//	//	strPatch, _ = sjson.SetBytes(strPatch, "0.path", "")
//	//}
//
//	w.current = jsonValue
//	w.outValue = data
//
//	return BatchMessageBytes(strPatch)
//}

func (w *patchWrapper) Out(data core.Data) ProtocolMessage {
	patch, err := jsondiff.Compare(w.outValue, data)
	if err != nil {
		panic(err)
	}

	if patch == nil {
		return nil
	}

	strPatch, _ := json.Marshal(patch)
	if len(strPatch) == 0 {
		return BatchMessage("[]")
	}

	w.outValue = data
	return BatchMessageBytes(strPatch)
}

func NewPatchWrapper() Wrapper {
	return &patchWrapper{
		value: "{}",
	}
}
