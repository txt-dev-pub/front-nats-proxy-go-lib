package treenityProtocol

import (
	"context"
	"errors"
	"fmt"
	"github.com/google/uuid"
	core "gitlab.com/txt-dev-pub/treenity-core"
	"go.uber.org/zap"
	"runtime/debug"
	"sync"
)

type Listen map[string]Socket

type ListenStorage struct {
	url     string
	sockets Listen
	options ListenOptions
}

type baseSubServer struct {
	logger     *zap.Logger
	listen     map[string]*ListenStorage
	subscriber Subscriber
	sender     Sender
	sync.RWMutex
}

func NewBaseSubServer(logger *zap.Logger, subscriber Subscriber, sender Sender) ISubscriptionServer {
	return &baseSubServer{
		logger:     logger,
		listen:     map[string]*ListenStorage{},
		sender:     sender,
		subscriber: subscriber,
	}
}

func (s *baseSubServer) runInput(ctx context.Context, subId string, val chan core.Data, cancel context.CancelFunc, wrapper Wrapper) {
	ch := make(chan ProtocolMessage)
	go s.subscriber(ctx, subId, ch)

	for {
		select {
		case msg := <-ch:
			s.logger.Info(
				"Server IN message",
				zap.String("subId", subId),
				zap.String("op", msg.Op()),
			)

			if msg == nil {
				continue
			}

			switch msg.Op() {
			case SubscriptionMessageType:
				continue
			case UnsubscriptionMessageType:
				fmt.Println("🤙 unsub")
				cancel()
			default:
				fmt.Println("🤙 new val", msg)
				val <- wrapper.In(msg)
			}
		}
	}
}

func (s *baseSubServer) runOutput(ctx context.Context, retId string, localSubId string, val chan core.Data, subcli, subsrv bool, cancel context.CancelFunc, wrapper Wrapper) {
	_msg := <-val

	if _msg.IsError() {
		err := _msg.GetError()
		s.sender(retId, Unsub(&err, &localSubId))
		//cancel()
		return
	}

	//msgStr, _ := json.Marshal(_msg)
	err := s.sender(retId, RetUpdate([]string{localSubId}, wrapper.Out(_msg).AsString(), subcli, subsrv).(ProtocolMessage))
	if err != nil {
		if err.Error() != "nats: timeout" {
			fmt.Printf("🛑 serverOutput %s\n", err)
			panic("Here2")
			cancel()
			return
		}
	}

	if !subsrv {
		fmt.Println("⚠️ server subscribe is false")
		return
	}

	for {
		select {
		case msg, ok := <-val:
			if !ok {
				return
			}

			if _msg.IsError() {
				err := _msg.GetError()
				s.sender(retId, Unsub(&err, &localSubId))
				panic("Here3")
				cancel()
				return
			}

			err := s.sender(retId, wrapper.Out(msg))
			if err != nil {
				panic(err)
				cancel()
			}
		case <-ctx.Done():
			return
		}
	}
}

func (s *baseSubServer) Listen(ctx context.Context, url string, opts ListenOptions, inputCh chan ProtocolMessage) (chan Socket, error) {
	s.RLock()
	if _, ok := s.listen[url]; ok {
		s.RUnlock()
		return nil, errors.New("sub-server.listen.already-listening")
	}
	s.RUnlock()

	// Resolve start
	listen := ListenStorage{
		url:     url,
		options: opts,
		sockets: make(Listen),
	}
	s.Lock()
	s.listen[url] = &listen
	s.Unlock()

	source := make(chan Socket)

	go func() {
		defer func() {
			if err := recover(); err != nil {
				fmt.Println("🛑 Server: ", err)
				debug.PrintStack()
			}
		}()

		go s.subscriber(ctx, url, inputCh)

		for {
			select {
			case msg, ok := <-inputCh:
				// If a subscriber's channel is closed, we should closeFn all publications
				if !ok {
					for _, pub := range listen.sockets {
						pub.closeFn()
					}
				}

				if msg == nil {
					s.logger.Warn("Received empty message")
					continue
				}

				switch msg.Op() {
				case SubscriptionMessageType:
					instanceCtx, cancelInstance := context.WithCancel(ctx)
					localSubId := fmt.Sprintf("S-%s", uuid.New().String())
					wrapper := opts.WrapperBuilder()
					retId := msg.RetId()

					//
					subcli := listen.options.Subcli && msg.Subcli()
					subsrv := listen.options.Subsrv && msg.Subsrv()

					fmt.Println("✅", listen.options.Subcli, msg.Subsrv(), listen.options.Subsrv, msg.Subcli())

					input := make(chan core.Data, 30)
					output := make(chan core.Data, 30)

					if subcli {
						go s.runInput(instanceCtx, localSubId, input, cancelInstance, wrapper)
					}

					go s.runOutput(instanceCtx, retId, localSubId, output, subcli, subsrv, cancelInstance, wrapper)

					url := baseMsg(msg.AsString()).getUrl()
					initialValue := baseMsg(msg.Value())

					pub := Socket{
						ctx:     instanceCtx,
						closeFn: cancelInstance,
						id:      localSubId,
						source:  input,
						sink:    output,
						url:     url,
						Value:   initialValue,
						wrapper: wrapper,
						opts:    opts,
					}

					s.Lock()
					listen.sockets[retId] = pub
					s.Unlock()
					source <- pub

					go func() {
						<-instanceCtx.Done()
						fmt.Println("🛑 Instance context was canceled")

						s.Lock()
						if _, ok := listen.sockets[retId]; ok {
							delete(listen.sockets, retId)
						}
						s.Unlock()

						if subsrv {
							s.sender(retId, Unsub(nil, nil))
						}

						close(input)
						close(output)

						s.logger.Info(
							"Removed subscriber",
							zap.Int("count", len(listen.sockets)),
						)

					}()
				//default:
				//	panic("Alarm!")
				case UnsubscriptionMessageType:
					s.sender(*msg.Sub(), msg)

					// TODO Duplicate
					close(source)
					close(inputCh)

					s.Lock()
					if _, ok := listen.sockets[url]; ok {
						delete(listen.sockets, url)
					}
					s.Unlock()

					s.logger.Info(
						"Method has been closed",
						zap.String("url", url),
					)

					return
				}

				// TODO No need
			case <-ctx.Done():
				close(source)
				close(inputCh)

				s.Lock()
				if _, ok := listen.sockets[url]; ok {
					delete(listen.sockets, url)
				}
				s.Unlock()

				s.logger.Info(
					"Method has been closed",
					zap.String("url", url),
				)

				return
			}
		}
	}()

	return source, nil
}

func (s *baseSubServer) Fallback(ctx context.Context, url string, ch chan ProtocolMessage) (chan Socket, error) {
	panic("No implemented")
}
