package treenityProtocol

import (
	"encoding/json"
	"github.com/tidwall/gjson"
	core "gitlab.com/txt-dev-pub/treenity-core"
)

type simpleWrapper struct {
}

func parse(value string) core.Data {
	switch v := gjson.Parse(value).Value().(type) {
	case map[string]any:
		return v
	case any, []any:
		return patchValue("{}", value)
	default:
		return core.Data{
			"value": v,
		}
	}
}

func (w *simpleWrapper) In(msg ProtocolMessage) core.Data {
	return parse(msg.Value())
}

func (w *simpleWrapper) Out(data core.Data) ProtocolMessage {
	val, err := json.Marshal(data)
	if err != nil {
		// TODO Check it
		return ErrorMsg(err.Error())
	}

	return ReplaceMessage("", string(val))
}

func NewSimpleWrapper() Wrapper {
	return &simpleWrapper{}
}
