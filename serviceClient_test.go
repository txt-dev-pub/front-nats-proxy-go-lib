package treenityProtocol

import (
	"encoding/json"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	core "gitlab.com/txt-dev-pub/treenity-core"
	"testing"
)

type ServerClientSuite struct {
	suite.Suite
	serverClient ServiceClient
}

func TestServerClientSuite(t *testing.T) {
	suite.Run(t, new(ServerClientSuite))
}

// Apply one deployment config
func (s *ServerClientSuite) Test_serverClientGetParams() {
	valueParams := `{
           "param1": "value1",
           "param2": "value2"
       }`

	var params core.Data

	err := json.Unmarshal([]byte(valueParams), &params)
	require.NoError(s.T(), err)

	v := serviceClient{
		params: params,
	}
	result := v.GetParams()

	require.Equal(s.T(), result.Get("param1"), "value1")
	require.NoError(s.T(), nil)
}

// If params == nil
func (s *ServerClientSuite) Test_serverClientNoParams() {
	v := serviceClient{}

	result := v.GetParams()

	require.Nil(s.T(), result)
}

// If params == nil and params get by key
func (s *ServerClientSuite) Test_serverClientNoParamsAndGet() {
	v := serviceClient{}

	result := v.GetParams().GetString("params1")

	require.NotNil(s.T(), result)
	require.Equal(s.T(), result, "")
}
