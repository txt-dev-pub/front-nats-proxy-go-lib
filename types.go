package treenityProtocol

import (
	"context"
	"fmt"
	"github.com/tidwall/gjson"
	"github.com/tidwall/sjson"
	core "gitlab.com/txt-dev-pub/treenity-core"
	"strings"
)

const (
	SubscriptionMessageType   = "sub"
	UnsubscriptionMessageType = "unsub"
	ReturnMessageType         = "ret"
	PatchedMessageType        = "batch"
)

type SubOptions interface {
	Subcli() bool
	Subsrv() bool
}

type SubOptionsObj struct {
	Subcli bool
	Subsrv bool
}

type SubItem interface {
	Sub() *string
	Error() *string
}

type ProtocolMsg interface {
	Op() string
	Path() string
	RetId() string
	Value() string
}

type BaseMsg = ProtocolMsg

type SubUnSubUpdate interface {
	BaseMsg
	SubItem
	SubOptions
}

type ProtocolMessage interface {
	SubUnSubUpdate
	SubOptions
	AsString() string
	AsBytes() []byte
	ValueAsParams() core.Data
}

type baseMsg string

func (m baseMsg) setUrl(url string) (baseMsg, error) {
	val, err := sjson.Set(string(m), "__url", url)
	return baseMsg(val), err
}

func (m baseMsg) getUrl() string {
	return gjson.Get(string(m), "__url").String()
}

func (m baseMsg) Path() string {
	return gjson.Get(string(m), "path").String()
}

func (m baseMsg) Value() string {
	return gjson.Get(string(m), "value").String()
}

func (m baseMsg) Op() string {
	return gjson.Get(string(m), "op").String()
}

func (m baseMsg) Sub() *string {
	path := strings.SplitN(m.Path()[1:], "/", 1)
	if len(path) > 0 {
		return &path[0]
	}

	return nil
}

func (m baseMsg) RetId() string {
	path := strings.SplitN(m.Path()[1:], "/", 1)
	if len(path) > 0 {
		return path[0]
	}

	return ""
}

func (m baseMsg) Error() *string {
	value := gjson.Get(string(m), "error")
	if value.Exists() {
		res := value.String()
		return &res
	}

	return nil
}

func (m baseMsg) Subcli() bool {
	return gjson.Get(string(m), "subcli").Bool()
}

func (m baseMsg) Subsrv() bool {
	return gjson.Get(string(m), "subsrv").Bool()
}

func (m baseMsg) AsString() string {
	return string(m)
}

func (m baseMsg) AsBytes() []byte {
	return []byte(m)
}

func (m baseMsg) ValueAsParams() core.Data {
	res, ok := gjson.Parse(string(m)).Value().(map[string]interface{})
	if !ok {
		return core.Data{}
	}

	return res
}

type SubRetUpdate interface {
	BaseMsg
	SubOptions
}

//func newSubRetFromParams(params ProtocolMessage) ProtocolMessage {
//	return params
//}
//
//func parseRetMsg(m ProtocolMessage) ProtocolMessage {
//	return baseMsg(m.Value())
//}

func CreatePath(items []string) string {
	return "/" + strings.Join(items, "/")
}

//---------------------------------------------------
// Return update message ----------------------------
//---------------------------------------------------

func RetUpdate(path []string, value string, subcli, subsrv bool) SubRetUpdate {
	msg := fmt.Sprintf("{\"op\":\"ret\",\"subcli\":%t,\"subsrv\":%t}", subcli, subsrv)
	msg, _ = sjson.Set(msg, "path", CreatePath(path))
	if len(value) == 0 {
		msg, _ = sjson.Set(msg, "value", value)
	} else {
		msg, _ = sjson.SetRaw(msg, "value", value)
	}
	return baseMsg(msg)
}

//---------------------------------------------------
// Error message ----------------------------
//---------------------------------------------------

func ErrorMessage(value error) ProtocolMessage {
	return baseMsg(fmt.Sprintf("{\"op\":\"error\",\"value\":\"%s\"}", value.Error()))
}

func ErrorMsg(value string) ProtocolMessage {
	return baseMsg(fmt.Sprintf("{\"op\":\"error\",\"value\":\"%s\"}", value))
}

//---------------------------------------------------
// Sub message --------------------------------------
//---------------------------------------------------

func subUpdate(subId *string, value string, subcli, subsrv bool) ProtocolMessage {
	msg := fmt.Sprintf("{\"op\":\"sub\",\"subcli\":%t,\"subsrv\":%t}", subcli, subsrv)
	if subId != nil {
		msg, _ = sjson.Set(msg, "path", CreatePath([]string{*subId}))
	}
	if len(value) == 0 {
		msg, _ = sjson.Set(msg, "value", value)
	} else {
		msg, _ = sjson.SetRaw(msg, "value", value)
	}
	return baseMsg(msg)
}

//---------------------------------------------------
// Add message --------------------------------------
//---------------------------------------------------

func AddMessage(path string, value string) ProtocolMessage {
	msg, _ := sjson.Set("{\"op\":\"add\"}", "path", path)
	if len(value) == 0 {
		msg, _ = sjson.Set(msg, "value", value)
	} else {
		msg, _ = sjson.SetRaw(msg, "value", value)
	}
	return baseMsg(msg)
}

//---------------------------------------------------
// Replace message --------------------------------------
//---------------------------------------------------

func ReplaceMessage(path string, value string) ProtocolMessage {
	msg, _ := sjson.Set("{\"op\":\"replace\"}", "path", path)
	if len(value) == 0 {
		msg, _ = sjson.Set(msg, "value", value)
	} else {
		msg, _ = sjson.SetRaw(msg, "value", value)
	}
	return baseMsg(msg)
}

//---------------------------------------------------
// Remove message --------------------------------------
//---------------------------------------------------

func RemoveMessage(path string, value string) ProtocolMessage {
	msg, _ := sjson.Set("{\"op\":\"remove\"}", "path", path)
	if len(value) == 0 {
		msg, _ = sjson.Set(msg, "value", value)
	} else {
		msg, _ = sjson.SetRaw(msg, "value", value)
	}
	return baseMsg(msg)
}

//---------------------------------------------------
// Ready message ------------------------------------
//---------------------------------------------------

func ReadyMessage(path string, value string) ProtocolMessage {
	msg, _ := sjson.Set("{\"op\":\"ready\"}", "path", path)
	if len(value) == 0 {
		msg, _ = sjson.Set(msg, "value", value)
	} else {
		msg, _ = sjson.SetRaw(msg, "value", value)
	}
	return baseMsg(msg)
}

//---------------------------------------------------
// Batch message ------------------------------------
//---------------------------------------------------

type BatchUpdate interface {
	ProtocolMsg
}

func BatchMessage(value string) ProtocolMessage {
	msg, _ := sjson.SetRaw("{\"op\":\"batch\"}", "value", value)
	return baseMsg(msg)
}

func BatchMessageBytes(value []byte) ProtocolMessage {
	msg, _ := sjson.SetRawBytes([]byte("{\"op\":\"batch\"}"), "value", value)
	return baseMsg(msg)
}

// ---------------------------------------------------
// Unsubscription message ----------------------------
// ---------------------------------------------------

func Unsub(error *string, subId *string) ProtocolMessage {
	msg := "{\"op\":\"unsub\"}"

	if subId != nil {
		msg, _ = sjson.Set(msg, "path", CreatePath([]string{*subId}))
		msg, _ = sjson.Set(msg, "sub", *subId)
	}

	if error != nil {
		msg, _ = sjson.Set(msg, "error", *error)
	}

	return baseMsg(msg)
}

//---------------------------------------------------
// Batch message ------------------------------------
//---------------------------------------------------

type Sink chan<- core.Data
type Source <-chan core.Data

type Socket struct {
	ctx     context.Context
	closeFn context.CancelFunc
	sink    chan core.Data
	source  chan core.Data
	url     string
	id      string
	error   error
	Value   ProtocolMessage
	wrapper Wrapper
	opts    ListenOptions
}

func (s *Socket) Source() Source {
	return s.source
}

func (s *Socket) Sink() Sink {
	return s.sink
}

func (s *Socket) Close() {
	s.closeFn()
}

func (s *Socket) Ctx() context.Context {
	return s.ctx
}

func (s *Socket) Url() core.Link {
	return core.Link(s.url)
}

type ListenOptions struct {
	SubOptionsObj
	WrapperBuilder WrapperBuilder
	// this listen is subscribable, if false - one-shot listener, only one value will be returnd back
	Sub bool `json:"sub,omitempty"`
	// if socket will broadcast (only one socket will be emitted for many connections
	Broadcast bool `json:"broadcast,omitempty"`
	// broadcast socket will be opened immediately, if no subscribers, packets will be lost
	Immediate bool `json:"immediate,omitempty"`
}

type ISubscriptionClient interface {
	// Subscribe to URL
	//
	// `url` - link on subscription, `params` - json object of named params for subscription
	//
	// returning array of
	Proxy(ctx context.Context, url string, params core.Data) Socket
	Subscribe(ctx context.Context, url string, params core.Data) Socket
	Request(ctx context.Context, url string, params core.Data) core.Data
	Sub(ctx context.Context, url string, subcli, subsrv, proxy bool, wBuilder WrapperBuilder) Socket
}

type ISubscriptionResolver interface {
	Resolve(ctx context.Context, url string) (ListenOptions, error)
}

type ISubscriptionServer interface {
	//Resolve(url string) (Socket, error)
	//SetFallback(fallback FallbackMethod)
	Listen(ctx context.Context, url string, opts ListenOptions, inputCh chan ProtocolMessage) (chan Socket, error)
}

// TODO Return context?
type Subscriber func(ctx context.Context, name string, ch chan ProtocolMessage)
type Sender func(name string, msg ProtocolMessage) error

type Handler[T any] interface {
	Subscribe(name string) Source
	Unsubscribe(name string)
	Publish(name string, msg T)
}

type WrapperBuilder func() Wrapper

type Wrapper interface {
	In(msg ProtocolMessage) core.Data
	Out(data core.Data) ProtocolMessage
}

type WrapperManager interface {
	Add(name string, b WrapperBuilder) error
	Get(name string) Wrapper
}

type Method struct {
	Instance       core.Runnable
	Subcli         bool
	Subsrv         bool
	WrapperBuilder WrapperBuilder `json:"-"`
}

type MethodLink struct {
	Url    string
	Subcli bool
	Subsrv bool
}

type M map[string]Method

func Resolver(resolver core.Runnable) core.Runnable {
	return func(ctx context.Context, in core.Data, out core.Pipe) {
		out <- core.Data{
			"runnable": resolver,
		}
	}
}

func CreateService(methods M, loop core.Runnable) core.Runnable {
	data := core.Data{}
	data.Set("methods", methods)

	return func(ctx context.Context, in core.Data, out core.Pipe) {
		out <- data
		if loop != nil {
			go loop(ctx, in, out)
		}
	}
}

func CreateSimpleMethod(runner core.Runnable) Method {
	return Method{
		Instance:       runner,
		Subcli:         false,
		Subsrv:         false,
		WrapperBuilder: NewSimpleWrapper,
	}
}

func CreatePatchMethod(runner core.Runnable, cli, srv bool) Method {
	return Method{
		Instance:       runner,
		Subcli:         cli,
		Subsrv:         srv,
		WrapperBuilder: NewPatchWrapper,
	}
}
