package main

import (
	"context"
	"fmt"
	"github.com/nats-io/nats.go"
	core "gitlab.com/txt-dev-pub/treenity-core"
	tp "gitlab.com/txt-dev-pub/treenity-protocol"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	nc, err := nats.Connect("0.0.0.0:4222")
	if err != nil {
		panic(err.Error())
	}

	client := tp.NewNatsClient(nc, 3600*time.Second, true)
	ctx, _ := context.WithCancel(context.TODO())

	resp1 := client.Request(ctx, "txt:/test.service/orders#resp", core.Data{"text": "Add me 1"})
	fmt.Printf("Client message: %s\n", resp1)

	resp2 := client.Request(ctx, "txt:/test.service/orders#resp", core.Data{"text": "Add me 2"})
	fmt.Printf("Client message: %s\n", resp2)

	resp3 := client.Request(ctx, "txt:/test.service/orders#resp", core.Data{"text": "Add me 3"})
	fmt.Printf("Client message: %s\n", resp3)

	resp4 := client.Request(ctx, "txt:/test.service/orders#resp", core.Data{"text": "Add me 4"})
	fmt.Printf("Client message: %s\n", resp4)

	// Press Ctrl+C to exit the process
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt, syscall.SIGTERM)
	<-ch
}
