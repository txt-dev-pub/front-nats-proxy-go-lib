package main

import (
	"context"
	"fmt"
	"github.com/nats-io/nats.go"
	core "gitlab.com/txt-dev-pub/treenity-core"
	tp "gitlab.com/txt-dev-pub/treenity-protocol"
	"time"
)

func main() {
	nc, err := nats.Connect("0.0.0.0:4222")
	if err != nil {
		panic(err.Error())
	}

	client := tp.NewNatsClient(nc, 3600*time.Second, true)
	ctx, cancelFn := context.WithCancel(context.TODO())

	go func() {
		// TODO Data
		socket := client.Subscribe(ctx, "txt:/test.service/orders#test", core.Data{"text": "Add me"})
		// TODO Wrapper
		source := socket.Source()

		for {
			select {
			case <-ctx.Done():
				socket.Close()
				return

			case msg := <-source:
				fmt.Printf("Client message: %s\n", msg)
			}
		}
	}()

	fmt.Println("Waiting unsub")
	time.Sleep(60 * time.Second)

	cancelFn()
}
