package treenityProtocol

import (
	"context"
	"fmt"
	"github.com/nats-io/nats.go"
	core "gitlab.com/txt-dev-pub/treenity-core"
	"go.uber.org/zap"
	"runtime/debug"
	"sync"
	"time"
)

type natWildcardHandler struct {
	handler natHandler
	subs    map[string]chan ProtocolMessage
	sync.RWMutex
}

func (n *natWildcardHandler) Subscribe(ctx context.Context, name string, ch chan ProtocolMessage) {
	if _, ok := n.subs[name]; ok {
		return
	}

	n.handler.Subscribe(ctx, name, ch)
}

func (n *natWildcardHandler) Unsubscribe(name string) {
	panic("implement me")
	n.RLock()
	delete(n.subs, name)
	n.RUnlock()
}

func (n *natWildcardHandler) Publish(name string, msg ProtocolMessage) error {
	return n.handler.Publish(name, msg)
}

func NewErrorFallback(server ISubscriptionServer) core.Runnable {
	return func(ctx context.Context, in core.Data, out core.Pipe) {
		//url := in.GetLink("url")
		errMsg := in.GetString("err")
		msg := in.Get("msg").(baseMsg)
		inCh := make(chan ProtocolMessage)

		subValue := msg.Sub()

		if subValue == nil {
			return
		}

		_, err := server.Listen(
			ctx,
			*subValue,
			ListenOptions{
				SubOptionsObj: SubOptionsObj{
					Subcli: false,
					Subsrv: false,
				},
				WrapperBuilder: NewSimpleWrapper,
			},
			inCh,
		)

		if err != nil {
			panic("Failed to create socket for fallback")
		}

		inCh <- Unsub(&errMsg, subValue)
	}
}

func MethodRunner(server ISubscriptionServer, resolver core.Runnable) core.Runnable {
	return func(ctx context.Context, in core.Data, _ core.Pipe) {
		url := in.GetLink("url")
		ch := in.Get("input").(chan ProtocolMessage)

		resolveData := core.Once(in, resolver)
		methodRef := core.NodeDataValue[Method](resolveData, "method")

		if methodRef == nil {
			panic(fmt.Sprintf("🛑🙀 Method \"%s\" not found\n", url))
		}

		socket, err := server.Listen(
			ctx,
			string(url),
			ListenOptions{
				SubOptionsObj: SubOptionsObj{
					Subcli: methodRef.Subcli,
					Subsrv: methodRef.Subsrv,
				},
				WrapperBuilder: methodRef.WrapperBuilder,
			},
			ch,
		)

		if err != nil {
			panic(err.Error())
		}

		go func() {
			defer func() {
				if err := recover(); err != nil {
					//source.Close()
					//close(out)
					fmt.Printf("🛑 sourse loop error: %s\n", err)
					debug.PrintStack()
				}
			}()

			for {
				select {
				case <-ctx.Done():
					fmt.Println("🛑 context was closed")
					return
				case source := <-socket:
					fmt.Println("↔️ started", url)
					method := *methodRef
					out := make(chan core.Data, 30)

					methodCtx, cancelMethod := context.WithCancel(ctx)
					wrapper := method.WrapperBuilder()

					params := wrapper.In(source.Value)
					params.Set("__url", url)
					params.Set("subcli", methodRef.Subcli)
					params.Set("subsrv", methodRef.Subsrv)

					go method.Instance(methodCtx, params, out)

					go func() {
						defer func() {
							if err := recover(); err != nil {
								//source.Close()
								//close(out)
								fmt.Printf("🛑 %s\n", err)
								debug.PrintStack()
								return
							}
						}()

						paramsCh := source.Source()
						outCh := source.Sink()

						for {
							select {
							case <-ctx.Done():
								fmt.Println("🛑 context was closed")
								cancelMethod()
								source.Close()
								close(out)
								return
							case msg, ok := <-paramsCh:
								if !ok {
									fmt.Println("🛑 Input channel is dead")
									return
								}

								// Cancel previous method
								fmt.Println("🤙 new params: ", msg, ok)
								cancelMethod()

								// Create new context
								methodCtx, cancelMethod = context.WithCancel(methodCtx)
								ch := make(chan core.Data)
								go method.Instance(methodCtx, msg, ch)
							case data, ok := <-out:
								if !ok {
									fmt.Println("🛑 Output channel is dead")
									return
								}

								outCh <- data
							}
						}
					}()
				}
			}
		}()
	}
}

func NatsServiceRunner(nc *nats.Conn, resolver core.Runnable) core.Runnable {
	return func(ctx context.Context, in core.Data, out core.Pipe) {
		lnk := in.GetLink("__url")
		node := in.GetNode(core.ItemKey)

		meta := node.MetaByName(lnk.Meta())
		if meta == nil {
			out <- core.NodeDataErr("meta-not-found")
			return
		}

		url := meta.GetString("url")
		timeout := meta.GetInt("timeout")

		if timeout == 0 {
			timeout = 10
		}

		NewNatsWildcardServer(ctx, nc, url, resolver, time.Duration(timeout)*time.Second, true)

		out <- core.NodeDataComplete()
	}
}

func NewNatsWildcardServer(ctx context.Context, nc *nats.Conn, baseUrl string, resolver core.Runnable, timeout time.Duration, debug bool) (ISubscriptionServer, error) {
	var logger *zap.Logger
	if debug {
		logger, _ = zap.NewDevelopment()
	} else {
		logger, _ = zap.NewProduction()
	}

	defer logger.Sync()

	h := &natWildcardHandler{
		subs: map[string]chan ProtocolMessage{},
		handler: natHandler{
			nc:      nc,
			logger:  logger,
			timeout: timeout,
			subs:    map[string]subObj{},
		},
	}

	server := NewBaseSubServer(logger, h.Subscribe, h.Publish)
	runner := MethodRunner(server, resolver)
	fallback := NewErrorFallback(server)

	sub, err := nc.Subscribe(baseUrl, func(msg *nats.Msg) {
		defer func() {
			if err := recover(); err != nil {
				// TODO: Check it
				delete(h.subs, msg.Subject)
				fallback(ctx, core.Data{"err": err, "msg": baseMsg(msg.Data)}, nil)
			}
		}()

		if len(msg.Reply) > 0 {
			nc.Publish(msg.Reply, []byte("Ok"))
		}

		logger.Info(
			"Nats wildcard message",
			zap.ByteString("data", msg.Data),
		)

		ch, ok := h.subs[msg.Subject]

		// If we already listen this
		if !ok {
			ch = make(chan ProtocolMessage)
			h.subs[msg.Subject] = ch

			runner(ctx, core.Data{"url": msg.Subject, "input": ch}, nil)
		}

		ch <- baseMsg(msg.Data)
	})

	go func() {
		<-ctx.Done()
		_ = sub.Drain()
	}()

	return server, err
}
