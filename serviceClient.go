package treenityProtocol

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/nats-io/nats.go"
	core "gitlab.com/txt-dev-pub/treenity-core"
	"net/url"
	"os"
	"strconv"
	"time"
)

type serviceClient struct {
	client   ISubscriptionClient
	callback string
	params   core.Data
	methods  M
}

type ServiceClient interface {
	Client() ISubscriptionClient
	Register(ctx context.Context, base string, name string, service core.Runnable) error
	GetParams() core.Data
	ProvideMethods(resolver core.Runnable)
}

func (s *serviceClient) GetParams() core.Data {
	return s.params
}

func (s *serviceClient) Client() ISubscriptionClient {
	return s.client
}

func (s *serviceClient) resolve(ctx context.Context, in core.Data, out core.Pipe) {
	_url := in.GetString("url")
	if method, ok := s.methods[_url]; ok {
		out <- core.Data{
			"method": method,
		}
		return
	}

	out <- core.NodeDataErr("method-not-found")
}

func (s *serviceClient) Register(ctx context.Context, base string, name string, service core.Runnable) error {
	var err error
	var pathValue string
	if len(name) == 0 {
		pathValue = base
	} else {
		pathValue, err = url.JoinPath(base, name)
	}

	if err != nil {
		return err
	}

	pipe := make(chan core.Data)
	go service(ctx, s.params, pipe)

	result := <-pipe
	methods := core.NodeDataValue[M](result, "methods")

	if methods == nil {
		return errors.New("no-methods")
	}

	for name, methodHandler := range *methods {
		methodName := fmt.Sprintf("%s#%s", pathValue, name)
		s.methods[methodName] = methodHandler
		fmt.Printf("\t➡️ Registred method: \033[32m%s\033[0m\n", methodName)
	}

	return nil
}

func (s *serviceClient) ProvideMethods(resolver core.Runnable) {
	if len(s.callback) > 0 {
		if resolver != nil {
			resolveData := core.Once(s.params, resolver)

			switch v := resolveData.Get("methods").(type) {
			case []MethodLink:
				s.client.Request(context.TODO(), s.callback, map[string]any{
					"methods": v,
				})
			}
		} else {
			// TODO: If no method we will send empty array
			s.client.Request(context.TODO(), s.callback, map[string]any{
				"methods": []MethodLink{},
			})
		}
	}
}

func NewServiceClient(resolver core.Runnable) ServiceClient {
	c := serviceClient{
		methods: make(M),
	}

	natsAddress := os.Getenv("NATS_SERVER")
	if len(natsAddress) == 0 {
		fmt.Println("[TXT] NATS_SERVER not set")
		return nil
	}

	debug := os.Getenv("DEBUG")
	c.callback = os.Getenv("TXT_CALLBACK_METHOD")
	if len(c.callback) == 0 {
		fmt.Println("[TXT] TXT_CALLBACK_METHOD not set")
	}

	baseUrl := os.Getenv("TXT_URL")
	if len(baseUrl) == 0 {
		fmt.Println("[TXT] BASE_URL not set")
		return nil
	}

	params := os.Getenv("TXT_SRV_PARAMS")
	if len(params) != 0 {
		var paramsParse core.Data
		err := json.Unmarshal([]byte(params), &paramsParse)
		if err != nil {
			c.params = paramsParse
		} else {
			c.params = make(core.Data)
		}
	}

	natsDuration := os.Getenv("NATS_SERVER_DURATION")
	timeoutReq := 100 * time.Second
	if natsDuration != "" {
		val, err := strconv.Atoi(natsDuration)
		if err == nil {
			timeoutReq = time.Duration(val) * time.Millisecond
		}
	}

	nc, err := nats.Connect(natsAddress)
	if err != nil {
		fmt.Println("[TXT] " + err.Error())
		wait := make(chan bool)
		go func() {
			for {
				time.Sleep(time.Second)
				nc, err = nats.Connect(natsAddress)
				if err != nil {
					fmt.Println("[TXT] Trying connect NATS")
					continue
				}

				wait <- true
			}
		}()

		<-wait
	}

	c.client = NewNatsClient(nc, timeoutReq, debug == "TRUE")

	if resolver == nil {
		resolver = c.resolve
	}

	_, err = NewNatsWildcardServer(
		context.Background(),
		nc,
		baseUrl,
		resolver,
		timeoutReq,
		debug == "TRUE",
	)

	if err != nil {
		panic(err)
	}

	return &c
}
