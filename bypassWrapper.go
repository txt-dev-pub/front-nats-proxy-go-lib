package treenityProtocol

import (
	"encoding/json"
	core "gitlab.com/txt-dev-pub/treenity-core"
)

type bypassWrapper struct {
}

func (w *bypassWrapper) In(msg ProtocolMessage) core.Data {
	return msg.ValueAsParams()
}

func (w *bypassWrapper) Out(data core.Data) ProtocolMessage {
	val, err := json.Marshal(data)
	if err != nil {
		return ErrorMsg(err.Error())
	}
	return baseMsg(val)
}

func BypassWrapper() Wrapper {
	return &bypassWrapper{}
}
