package treenityProtocol

import (
	"context"
	core "gitlab.com/txt-dev-pub/treenity-core"
)

func NewServiceMethodResolver(p MethodHolder) core.Runnable {
	return func(ctx context.Context, in core.Data, out core.Pipe) {
		methodName := in.GetString(core.MethodKey)

		if len(methodName) == 0 {
			out <- core.NodeDataErr("resolver.method-in-link.not-found")
			return
		}

		method, ok := p.GetMethod(methodName)
		if !ok {
			out <- core.NodeDataErr("runner.method.not-found")
			return
		}

		out <- core.Data{
			"method": method,
		}
	}
}

func MethodResolver(executor core.Executor) core.Runnable {
	return func(ctx context.Context, in core.Data, out core.Pipe) {
		url := in.GetLink("url")
		out <- executor.ExecuteLinkRes(url.SetContext("resolve"), nil)
	}
}
