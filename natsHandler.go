package treenityProtocol

import (
	"context"
	"github.com/nats-io/nats.go"
	"go.uber.org/zap"
	"sync"
	"time"
)

type subObj struct {
	s      *nats.Subscription
	cancel context.CancelFunc
}

type natHandler struct {
	logger  *zap.Logger
	nc      *nats.Conn
	subs    map[string]subObj
	timeout time.Duration
	sync.RWMutex
}

func (n *natHandler) Subscribe(ctx context.Context, name string, ch chan ProtocolMessage) {
	subCtx, cancelFn := context.WithCancel(ctx)

	n.logger.Info(
		"Subscribe started",
		zap.String("name", name),
	)

	sub, err := n.nc.Subscribe(name, func(msg *nats.Msg) {
		if len(msg.Reply) > 0 {
			err := n.nc.Publish(msg.Reply, []byte("Ok"))
			if err != nil {
				n.logger.Error(
					"Nats publish error",
					zap.String("subject", msg.Reply),
					zap.String("err", err.Error()),
				)
				panic("Nats publish error: " + err.Error())
			}
		}

		n.logger.Info(
			"Nats message",
			zap.String("name", name),
			zap.ByteString("data", msg.Data),
		)

		m := baseMsg(msg.Data)
		if m.Op() == "sub" {
			m, _ = m.setUrl(msg.Subject)
		}
		ch <- m

		n.logger.Info(
			"✅ Nats message processed",
			zap.String("name", name),
		)
	})

	// Dispose method
	go func() {
		<-subCtx.Done()

		n.logger.Info(
			"Nats topic disposed",
			zap.String("name", name),
		)

		n.RLock()
		n.subs[name].s.Drain()
		n.RUnlock()
	}()

	// On error
	if err != nil {
		n.logger.Error(
			"nats subscribe handled err",
			zap.String("err", err.Error()),
		)
		panic("Nats publish error: " + err.Error())
	}

	n.Lock()
	n.subs[name] = subObj{
		s:      sub,
		cancel: cancelFn,
	}
	n.Unlock()
}

func (n *natHandler) Unsubscribe(name string) {
	n.RLock()
	n.subs[name].cancel()
	n.RUnlock()
}

func (n *natHandler) Publish(name string, msg ProtocolMessage) error {
	if msg == nil {
		n.logger.Info("🙀️  Message is nil")
		return nil
	}

	data := msg.AsBytes()
	err := n.nc.Publish(name, data)
	n.logger.Info(
		"✉️  Message",
		zap.ByteString("msg", data),
	)
	return err
}

func NewNatsServer(nc *nats.Conn, timeout time.Duration, debug bool) (ISubscriptionServer, error) {
	var logger *zap.Logger
	if debug {
		logger, _ = zap.NewDevelopment()
	} else {
		logger, _ = zap.NewProduction()
	}

	defer logger.Sync()

	h := &natHandler{
		logger:  logger,
		nc:      nc,
		subs:    map[string]subObj{},
		timeout: timeout,
	}

	return NewBaseSubServer(logger, h.Subscribe, h.Publish), nil
}

func NewNatsClient(nc *nats.Conn, timeout time.Duration, debug bool) ISubscriptionClient {
	var logger *zap.Logger
	if debug {
		logger, _ = zap.NewDevelopment()
	} else {
		logger, _ = zap.NewProduction()
	}

	defer logger.Sync()

	h := &natHandler{
		logger:  logger,
		nc:      nc,
		subs:    map[string]subObj{},
		timeout: timeout,
	}
	return NewBaseSubClient(logger, h.Publish, h.Subscribe)
}
